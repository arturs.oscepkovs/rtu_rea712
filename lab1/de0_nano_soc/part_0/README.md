## Part 0. Demultiplexing circuit
*Purpose: Get acquinted with the overall Quartus project flow.*

1. Together with your lecturer start a new Quartus project and use VHDL to describe a 4-to-1 demultiplexer using the following pin configuration (Note that pin locations are determined by the chip package and specific PCB, i.e. pin locations differ from board to board).

2. Use the **RTL Viewer** tool to check if the circuit has been elaborated as expected. The tool is available under: *Tools - Netlist Viewers - RTL Viewer*. Further, program the FPGA and check if the circuit works as expected.
