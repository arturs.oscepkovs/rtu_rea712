## Part 2. "Noisy" circuit

*purpose: Facilitate awareness of the underlying analog nature of digital circuits*

Implement a "noisy" circuit described in the handout and check it for any side-effects. Note that the circuit itself is already provided to you. **Your task is to implement the clock divider (in "rtu" library).**

