library std;
library ieee;
library rtu;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu_test.procedures.all;


entity tb is
  generic(
    runner_cfg : string;
    RANDOMIZED_TEST_COUNT : natural := 100
  );
end entity;

architecture RTL of tb is
  constant CLK_PERIOD : time := 10 ns;
  constant TIMEOUT    : time := (2**16)*CLK_PERIOD;

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal clk, start, done   : std_logic := '0';
  signal divident, divisor  : std_logic_vector(15 downto 0) := (others => '0');
  signal quotient, reminder : std_logic_vector(15 downto 0) := (others => '0');

begin
  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.trivial_divider
  port map(
    clk      => clk,
    start    => start,
    divident => divident,
    divisor  => divisor,
    done     => done,
    quotient => quotient,
    reminder => reminder);

  -----------------------------------------------------------------------------
  -- Clock
  -----------------------------------------------------------------------------
  clk <= not clk after CLK_PERIOD/2;

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    variable random            : RandomPType;
    variable divident_var      : std_logic_vector(15 downto 0);
    variable divisor_var       : std_logic_vector(15 downto 0);
    variable quotient_expected : std_logic_vector(15 downto 0);
    variable reminder_expected : std_logic_vector(15 downto 0);

    ---------------------------------------------------------------------------
    -- Procedures
    ---------------------------------------------------------------------------
  begin
    test_runner_setup(runner, runner_cfg);
    random.InitSeed(random'instance_name);

    while test_suite loop
      if run("random_coverage") then
        for i in 0 to RANDOMIZED_TEST_COUNT-1 loop
          -- generate input vectors
          divident_var := random.RandSlv(divident_var'length);
          divisor_var  := random.RandSlv(divisor_var'length);

          -- generate stimuli
          if unsigned(divisor_var) = 0 then
            quotient_expected := (others => '0');
            reminder_expected := (others => '0');
          else
            quotient_expected := std_logic_vector(to_unsigned(
              to_integer(unsigned(divident_var))
              / to_integer(unsigned(divisor_var)),
              quotient_expected'length
            ));
            reminder_expected := std_logic_vector(to_unsigned(
              to_integer(unsigned(divident_var))
              mod to_integer(unsigned(divisor_var)),
              reminder_expected'length
            ));
          end if;

           
          divident <= divident_var;
          divisor  <= divisor_var;
          start    <= '1';
          wait until rising_edge(clk);
          start    <= '0';

          wait until rising_edge(clk) and done = '1' for TIMEOUT;
          check_sl(done, '1', "Checking if done signal has been asserted");
          check_slv(quotient, quotient_expected, "Checking quotient for "
            & integer'image(to_integer(unsigned(divident))) & "/"
            & integer'image(to_integer(unsigned(divisor))));
          check_slv(reminder, reminder_expected, "Checking reminder for "
            & integer'image(to_integer(unsigned(divident))) & "/"
            & integer'image(to_integer(unsigned(divisor))));
        end loop;
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
