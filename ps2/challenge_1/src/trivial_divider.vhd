--------------------------------------------------------------------------------
--! @file circular_shift_left.vhd
--------------------------------------------------------------------------------

library ieee;
library rtu;
use ieee.std_logic_1164.all;

--! @brief A multi-cycle divider.
--!
entity trivial_divider is
  port(
    clk      : in  std_logic; --! clock
    start    : in  std_logic; --! starts operation of the divider
    divident : in  std_logic_vector(15 downto 0);
    divisor  : in  std_logic_vector(15 downto 0);
    done     : out std_logic; --! indicates completed calculation
    quotient : out std_logic_vector(15 downto 0);
    reminder : out std_logic_vector(15 downto 0)
  );
end entity;

architecture RTL of trivial_divider is
begin
end architecture;
