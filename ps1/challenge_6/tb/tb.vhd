library std;
library ieee;
library rtu;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu.functions.all;
use rtu_test.procedures.all;


entity tb is
  generic(
    runner_cfg : string;
    DATA_WIDTH : natural := 16
  );
end entity;

architecture RTL of tb is

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal clk : std_logic := '0';
  signal i_data     : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal o_position : unsigned(log2c(DATA_WIDTH)-1 downto 0);
  signal o_valid    : std_logic;

begin
  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.leading_one_position_locater
  generic map(
    DATA_WIDTH => DATA_WIDTH
  )
  port map(
    i_data     => i_data,
    o_position => o_position,
    o_valid    => o_valid);

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    variable random            : RandomPType;

    ---------------------------------------------------------------------------
    -- Procedures
    ---------------------------------------------------------------------------
  begin
    test_runner_setup(runner, runner_cfg);
    random.InitSeed(random'instance_name);

    while test_suite loop
      if run("position_coverage") then
        -- no input ones condition
        i_data <= (others => '0');
        wait for 10 ns;
        check_sl(o_valid, '0', "Checking valid when no ones provided");
      
        -- every ones position
        for position in 0 to DATA_WIDTH-1 loop
          i_data <= std_logic_vector(to_unsigned(2**position, DATA_WIDTH));
          wait for 10 ns;
          check_sl(o_valid, '1',
            "Checking valid when ones are provided");
          check_unsigned(o_position, to_unsigned(position, DATA_WIDTH),
            "Checking output position value for input data " & to_hstring(i_data));
        end loop;
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
