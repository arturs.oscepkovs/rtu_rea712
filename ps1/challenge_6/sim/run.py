#!/usr/bin/env python3
from vunit import VUnit

# create VUnit project
prj = VUnit.from_argv()

# add OSVVM library
prj.add_osvvm()
prj.add_verification_components()

# add custom libraries
prj.add_library("rtu")
prj.add_library("rtu_test")

# add sources and testbenches
prj.library("rtu").add_source_file("../src/leading_one_position_locater.vhd")
prj.library("rtu").add_source_file("../tb/tb.vhd")
prj.library("rtu").add_source_file("../../../libs/rtu/pkg/functions.vhd")
prj.library("rtu_test").add_source_file("../../../libs/rtu_test/pkg/procedures.vhd")

# add tests
prj.library('rtu').test_bench("tb").test("position_coverage").add_config(
  name="data_width=32",
  generics=dict(
    DATA_WIDTH = 8))

prj.library('rtu').test_bench("tb").test("position_coverage").add_config(
  name="data_width=16",
  generics=dict(
    DATA_WIDTH = 16))

prj.library('rtu').test_bench("tb").test("position_coverage").add_config(
  name="data_width=17",
  generics=dict(
    DATA_WIDTH = 17))

prj.library('rtu').test_bench("tb").test("position_coverage").add_config(
  name="data_width=15",
  generics=dict(
    DATA_WIDTH = 15))


# run VUnit simulation
prj.main()
