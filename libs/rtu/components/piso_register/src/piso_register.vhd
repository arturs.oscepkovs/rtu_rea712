library ieee;
library rtu;
use ieee.std_logic_1164.all;

entity piso_register is
  port(
    clk      : in  std_logic;        --! clock signal
    rst      : in  std_logic;        --! asynchronous reset
    i_load   : in  std_logic;        --! load shift register with data
    i_enable : in  std_logic;        --! enable shift register
    i_data   : in  std_logic_vector; --! input data (parallel)
    o_data   : out std_logic         --! output data (serialized) bit 
  );
end entity;

architecture RTL of piso_register is
  -- retreive input data width using attributes (modern style)
  constant DATA_WIDTH : natural := i_data'length;

  signal data_reg, data_next : std_logic_vector(DATA_WIDTH-1 downto 0) := (others =>'0');
begin

  -- reg-state logic
  -- <TODO: your code goes here>

  -- next-state logic
  -- <TODO: your code goes here>

  -- outputs
  -- <TODO: your code goes here>
end architecture;
