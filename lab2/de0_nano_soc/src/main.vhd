library ieee;
library rtu;
library pll;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;
use rtu.data_types.all;

entity main is
  generic(
    PATTERN : pattern_t := PATTERN_UNIFORM_COLOR
    -- Task 1:  PATTERN_UNIFORM_COLOR
    -- Task 2:  PATTERN_GENERATED
    -- Task 3:  PATTERN_MEMORY
  );
  port(
    -- input to the PLL
    clk_50MHz : in  std_logic; -- 50 MHz quartz clock
    btn_reset : in  std_logic; -- push button used as a reset

	 -- VGA interrupt
	 i_interrupt : in  std_logic;
    -- VGA interface signals
    o_hsync     : out std_logic;
    o_vsync     : out std_logic;
    o_denable   : out std_logic;
    o_color_r   : out std_logic_vector(7 downto 0);
    o_color_g   : out std_logic_vector(7 downto 0);
    o_color_b   : out std_logic_vector(7 downto 0);
    o_video_clk : out std_logic;

    -- HDMI chip I2C interface
    io_sda : inout std_logic;
    io_scl : inout std_logic
  );
end entity;

architecture RTL of main is
  -- VGA interface (1024 x 768)
  constant VGA_CONFIG : vga_config_t := (
    HOR_DISPLAY     => 1024,
    HOR_FRONT_PORCH => 24,
    HOR_SYNC        => 136,
    HOR_BACK_PORCH  => 160,
    VER_DISPLAY     => 768,
    VER_FRONT_PORCH => 3,
    VER_SYNC        => 6,
    VER_BACK_PORCH  => 29 
  );

  -- VGA counter max values
  constant HOR_COUNTER_MAX_VALUE : natural := VGA_CONFIG.HOR_DISPLAY + 
    VGA_CONFIG.HOR_FRONT_PORCH + VGA_CONFIG.HOR_SYNC + 
    VGA_CONFIG.HOR_BACK_PORCH - 1;
  constant VER_COUNTER_MAX_VALUE : natural := VGA_CONFIG.VER_DISPLAY + 
    VGA_CONFIG.VER_FRONT_PORCH + VGA_CONFIG.VER_SYNC + 
    VGA_CONFIG.VER_BACK_PORCH - 1;

  -- VGA counter widths
  constant HOR_COUNTER_WIDTH : natural := log2c(HOR_COUNTER_MAX_VALUE+1);
  constant VER_COUNTER_WIDTH : natural := log2c(VER_COUNTER_MAX_VALUE+1);

  -- PLL output
  signal clk_65Mhz : std_logic;
  signal clk_20Khz : std_logic;

  -- COUNTER Generator output
  signal counter_hor : unsigned(HOR_COUNTER_WIDTH-1 downto 0);
  signal counter_ver : unsigned(VER_COUNTER_WIDTH-1 downto 0);

  -- SYNC Generator output
  signal sync_hor, sync_ver, sync_denable : std_logic;
  signal sync_counter_hor : unsigned(HOR_COUNTER_WIDTH-1 downto 0);
  signal sync_counter_ver : unsigned(VER_COUNTER_WIDTH-1 downto 0);

  -- HDMI Initializer
  signal adv_sda_oe, adv_sda_in, adv_sda_out : std_logic;
  signal adv_scl_oe, adv_scl_in, adv_scl_out : std_logic;

begin

  -- Instantiate PLL component with the configured output frequency
  -- of 65 MHz
  PLL_65MHz: entity pll.pll
  port map (
    refclk   => clk_50MHz,
    rst      => '0', --not enable_reg, --btn_reset,
    outclk_0 => clk_65Mhz);

  -- Instantiate COUNTER generator
  COUNTER_GEN: entity rtu.counter_generator
  generic map(
    HOR_COUNTER_MAX_VALUE => HOR_COUNTER_MAX_VALUE,
    VER_COUNTER_MAX_VALUE => VER_COUNTER_MAX_VALUE)
  port map(
    clk        => clk_65Mhz,
    o_hcounter => counter_hor,
    o_vcounter => counter_ver);

  -- Instantiate SYNC generator
  SYNC_GEN: entity rtu.sync_generator
  generic map(VGA_CONFIG => VGA_CONFIG)
  port map(
    clk         => clk_65Mhz,
    i_hcounter  => counter_hor,
    i_vcounter  => counter_ver,
    o_denable   => sync_denable,
    o_hsync     => sync_hor,
    o_vsync     => sync_ver,
    o_hcounter  => sync_counter_hor,
    o_vcounter  => sync_counter_ver);
  
  -- Instantiate PATTERN generator
  PATTERN_GEN: entity work.pattern_generator
  generic map(
    VGA_CONFIG => VGA_CONFIG,
    PATTERN    => PATTERN)
  port map(
    clk        => clk_65Mhz,
    i_denable  => sync_denable,
    i_hsync    => sync_hor,
    i_vsync    => sync_ver,
    i_hcounter => sync_counter_hor,
    i_vcounter => sync_counter_ver,
    o_denable  => o_denable,
    o_hsync    => o_hsync,
    o_vsync    => o_vsync,
    o_color_r  => o_color_r,
    o_color_g  => o_color_g,
    o_color_b  => o_color_b);
	 
  CLK_DIVIDER: entity work.clock_divider_pulse
  generic map(
    DIVISION_RATIO => 1250
  )
  port map(
    clk_in  => clk_50MHz,
    clk_out => clk_20Khz
  );

  -- Instantiate HDMI chip initializer
  HDMI_INIT: entity rtu.adv7513_initializer
  port map(
    -- input to the PLL
    clk => clk_20Khz,
    rst => btn_reset,
    -- reconfiguration
    reconfigure => not i_interrupt,
    -- physical interface with I2C
    o_sda_oe => adv_sda_oe,
    i_sda    => adv_sda_in,
    o_sda    => adv_sda_out,
    o_scl_oe => adv_scl_oe,
    i_scl    => adv_scl_in,
    o_scl    => adv_scl_out);

  -- output video clock
  o_video_clk <= clk_65Mhz;

  -- output tristate buffers
  adv_sda_in <= io_sda;
  adv_scl_in <= io_scl;
  io_sda <= adv_sda_out when adv_sda_oe = '1' else
            'Z';
  io_scl <= adv_scl_out when adv_scl_oe = '1' else
            'Z';

end architecture;
